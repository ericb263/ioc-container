## Simple IOC program

A simple IOC Container. Priority was placed on brevity and the ability for the program to be easily extended upon. 
The test class shows a simple example of how the resolve method can be used through a russian stacking doll real world example. 

#Possible Extensions

	#Registration method
		Would allow the users to register types to the container. This would grant the user greater control over the resolve method.
		Implementation would include a (private) Map of types. During the resolve method, the container would check this map for any user settings. 
		
	#Array support
		Addition of array support for the resolve method. Javascript/Typescript allow the users to provide an inital (non-limiting) length of an array which could
		be used as a limit for the resolve method. 

