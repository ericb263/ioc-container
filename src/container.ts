import "reflect-metadata";
const metadataKey = "design:paramtypes";//Constructor metadata key

export interface Type<type>{
    new(... args: any): type;
}

export type decorator<type> = (node: type) => void;
export let Decorator = () : decorator<Type<any>> =>{
    return (node: Type<any>) => {};
};

/*
 * Container class for resolving objects and registering types. 
 */
export let Container = new class{
    //Method: Resolve method that takes in a node
    resolve<type>(node: Type<any>): type {
      let retval;
      if(node == Array){
        //TODO (if enough time) add code for filling out arrays of dependencies
      }
      else{
        let metadatas = Reflect.getMetadata(metadataKey, node);//Use reflect to obtain the metadata. This helps us determine if we need to populate further nodes or not. 
        if(metadatas == null){
          //Indicates this node is the last link in the object tree
          retval = new node();
        }
        else{
          //Indicates there are more nodes to be filled in the tree
          retval = new node(... metadatas.map(metadata => this.resolve<any>(metadata)));
        }
      }
      return retval;
    }

    //TODO (if enough time): add a register method for registering specific object types
    register(node: Type<any>){}
};