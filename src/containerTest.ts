import {Decorator, Container} from './container'

/*
 * Simple example showing the container resolve method in the form of Matryoshka (Russian) Stacking Dolls. Hierarchy goes as follows:
 *  Olga - Top doll
 *    Natasha, Middle doll
 *      Helena, Bottom doll
 *        Candy, Chocolate
 *        Candy, Chocolate
 *        Candy, Toffee
 * 
 * Note: I was working on adding support for arrays of dependencies. Would be a great area of expansion for this project. 
 * Note: I was also planning on adding registration for the container. This would be a map on the container object that could hold types the user registers to it. 
 */

class mishkaCandy{
  private taste = "Chocolate";

  public getTaste(){
    return this.taste;
  }
}

class korovkaCandy{
  private taste = "Toffee";

  public getTaste(){
    return this.taste;
  }
}

@Decorator()
class bottomDoll{
  private name : string = "Helena";
  constructor(public candy1: mishkaCandy, public candy2: mishkaCandy, public candy3: korovkaCandy){
  }
  getName(){
    return this.name
  }
  setName(name : string){
    this.name = name;
  }
}

@Decorator()
class middleDoll{
  private name : string = "Natasha";
  constructor(public doll: bottomDoll){
  }
  getName(){
    return this.name
  }
  setName(name : string){
    this.name = name;
  }
}

@Decorator()
class topDoll{
  private name : string = "Olga";
  constructor(public doll: middleDoll) {
  }
  getName(){
    return this.name
  }
  setName(name : string){
    this.name = name;
  }
}

//Shows nested injection in the form of stacking dolls. 
const doll = Container.resolve<topDoll>(topDoll);
console.log(doll.getName()); //Olga, first doll
console.log(doll.doll.getName()); //Natasha, second doll
console.log(doll.doll.doll.getName()); // Helena, third doll
let finalDoll = doll.doll.doll;
console.log(finalDoll.candy1.getTaste(), finalDoll.candy2.getTaste(), finalDoll.candy3.getTaste()); //Candy in the final doll: Chocolate, Chocolate, Toffee